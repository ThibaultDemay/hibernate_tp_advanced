package com.company.helper;

import com.company.exception.AlreadyExistsException;
import com.company.model.*;
import com.company.repository.*;

import java.util.*;

public class DataBaseInserter {
    private PersonRepository personRepository = new PersonRepository();
    private BookRepository bookRepository = new BookRepository();
    private BandRepository bandRepository = new BandRepository();
    private StyleRepository styleRepository = new StyleRepository();
    private CdRepository cdRepository = new CdRepository();
    private CategoryRepository categoryRepository = new CategoryRepository();
    private DvdRepository dvdRepository = new DvdRepository();
    private CommandRepository commandRepository = new CommandRepository();
    private ArticleRepository articleRepository = new ArticleRepository();

    public DataBaseInserter() {

    }

    public void fillDataBase() throws AlreadyExistsException {
        //region CREATION DES PERSONS

        personRepository.insert(new Person("Lovecraft", "Howard Philips"));
        personRepository.insert(new Person("Asimov", "Isaac"));
        personRepository.insert(new Person("Damasio", "Alain"));
        personRepository.insert(new Person("Dick", "Philip K"));
        personRepository.insert(new Person("Orwell", "Georges"));
        personRepository.insert(new Person("Rand", "Ayn"));
        personRepository.insert(new Person("Cobain", "Kurt"));
        personRepository.insert(new Person("Clapton", "Eric"));
        personRepository.insert(new Person("Reinhardt", "Django"));
        personRepository.insert(new Person("Elington", "Duke"));
        personRepository.insert(new Person("Knopfler", "Mark"));
        personRepository.insert(new Person("Fitzgerald", "Ella"));
        personRepository.insert(new Person("London", "Julie"));
        personRepository.insert(new Person("King", "BB"));
        personRepository.insert(new Person("Carpenter", "John"));
        personRepository.insert(new Person("Scott", "Ridley"));
        personRepository.insert(new Person("Weaver", "Sigourney"));
        personRepository.insert(new Person("Russel", "Kurt"));
        personRepository.insert(new Person("David", "Keith"));
        personRepository.insert(new Person("Cage", "Nicolas"));
        //endregion

        //region CREATION DES BOOKS
        List<Person> authors = new ArrayList<>();
        authors.add(personRepository.findById(1));
        bookRepository.insert(new Book("La couleur tombée du ciel", authors, (float) (10.00)));
        authors = new ArrayList<>();
        authors.add(personRepository.findById(3));
        bookRepository.insert(new Book("Les furtifs", authors, (float) (15.00)));
        authors = new ArrayList<>();
        authors.add(personRepository.findById(4));
        authors.add(personRepository.findById(5));
        bookRepository.insert(new Book("Blade Runner et la ferme des animaux", authors, (float) (35.00)));
        //endregion

        //region CREATION DES BANDS

        List<Person> members = new ArrayList<Person>();
        members.add(personRepository.findById(10));
        members.add(personRepository.findById(8));
        members.add(personRepository.findById(11));
        members.add(personRepository.findById(12));
        members.add(personRepository.findById(13));
        members.add(personRepository.findById(14));
        bandRepository.insert(new Band("Une belle brochette de blues women & men", members));

        members = new ArrayList<Person>();
        members.add(personRepository.findById(1));
        members.add(personRepository.findById(9));
        bandRepository.insert(new Band("Lovecraft & Django Reinhardt", members));
        //endregion

        //region CREATION DES STYLES
        styleRepository.insert(new Style("Blues"));
        styleRepository.insert(new Style("Jazz"));
        styleRepository.insert(new Style("Death Metal Papillon"));
        //endregion

        //region CREATION DES CD
        List<Style> styles1 = new ArrayList<>();
        styles1.add(styleRepository.findById(1));
        List<Band> bands = new ArrayList<>();
        bands.add(bandRepository.findById(1));
        cdRepository.insert(new Cd("Best Of Blues", (float) (50.00), bands, styles1));

        styles1 = new ArrayList<>();
        styles1.add(styleRepository.findById(1));
        styles1.add(styleRepository.findById(2));
        styles1.add(styleRepository.findById(3));
        bands = new ArrayList<>();
        bands.add(bandRepository.findById(1));
        bands.add(bandRepository.findById(2));

        cdRepository.insert(new Cd("L'appel de Cthulhu en La mineur 7 5b", (float) (70.00), bands, styles1));

        //endregion

        //region CREATION DES CATEGORIES
        categoryRepository.insert(new Category("Science Fiction"));
        categoryRepository.insert(new Category("Horreur"));
        categoryRepository.insert(new Category("Documentaire"));

        //endregion

        //region CREATION DES DVDs :

        List<Person> actors = new ArrayList<>();
        actors.add(personRepository.findById(18));
        actors.add(personRepository.findById(19));
        actors.add(personRepository.findById(1));
        Person director = personRepository.findById(15);
        Category category = categoryRepository.findById(2);
        dvdRepository.insert(new Dvd("The Thing", (float) (150.00), actors, director, category));

        actors = new ArrayList<>();
        actors.add(personRepository.findById(17));
        director = personRepository.findById(16);
        category = categoryRepository.findById(1);
        dvdRepository.insert(new Dvd("Alien, le huitième passager", (float) (200.00), actors, director, category));

        actors = new ArrayList<>();
        actors.add(personRepository.findById(20));
        director = personRepository.findById(1);
        category = categoryRepository.findById(1);
        dvdRepository.insert(new Dvd("La couleur tombée du ciel, le film !", (float) (300.00), actors, director, category));

        //endregion

        //region  CREATION DES COMMANDES :

        // Une première commande avec 5 livres 1, 10 livres 2, et 3 cd 4
        Command command = new Command("Commande 1",
                new CommandArticle(articleRepository.findById(1),5), // 5Couleurs*10 LIVRE 50
                new CommandArticle(articleRepository.findById(2),10), // 10Lesfurtis*15 LIVRE 150
                new CommandArticle(articleRepository.findById(4),3) // 3BestofBlues*50 150 CD
        );
        // Une seconde commande avec 12 livres 3, 50 dvd 7, et 15 cd 5
        Command command2 = new Command("Commande 2",
                new CommandArticle(articleRepository.findById(3),4), // 4Bladerruner*35 LIVRE 140
                new CommandArticle(articleRepository.findById(7),8), // 8Alien* 200 DVD 1600
                new CommandArticle(articleRepository.findById(8),7) // 7 CouleurFilm* 300 DVD 2100
        );

        // En tout, on en a pour 340 de livres, 150 de cd et 3700 de dvd
        commandRepository.insert(command);
        commandRepository.insert(command2);


        //endregion

        // REQUETE AVANCEE 1 : LA TOTALITE DES OEUVRES D'UN ARTISTE
        System.out.println("DB remplie !");



    }

}
