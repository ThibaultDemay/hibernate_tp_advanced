package com.company.repository;

import com.company.exception.AlreadyExistsException;
import com.company.helper.DataBaseHelper;
import com.company.model.Article;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.company.repository.BookRepository;
import com.company.repository.CdRepository;
import com.company.repository.DvdRepository;


public class ArticleRepository extends GenericRepository {

    BookRepository bookRepository = new BookRepository();
    CdRepository cdRepository = new CdRepository();
    DvdRepository dvdRepository = new DvdRepository();

    public Article insert(Article article) throws AlreadyExistsException {
        EntityManager entityManager = getEntityManager();
        //Article existing = findById(article.getId());
        List<Article> existingArticles = findByLabel(article.getLabel());
        if (!existingArticles.isEmpty()) {
            throw new AlreadyExistsException("The article labeled "+article.getLabel() + " already exists.");
        }
        DataBaseHelper.beginTransaction(entityManager);
        entityManager.persist(article);
        DataBaseHelper.commitTransactionAndClose(entityManager);
        return article;
    }


    public Article update(Article article) {
        EntityManager entityManager = getEntityManager();
        DataBaseHelper.beginTransaction(entityManager);
        entityManager.merge(article);
        DataBaseHelper.commitTransactionAndClose(entityManager);
        return article;
    }



    /**
     * Finds all articles.
     * @return A list containing all the articles.
     */
    public List<Article> findAll() {
        return getEntityManager().createQuery("from Article order by id", Article.class).getResultList();
    }


    /**
     * Finds a article by its id.
     * @return The matching article, otherwise null.
     * @throws SQLException
     */
    public Article findById(int id) {
        return getEntityManager().find(Article.class, id);
    }

    /**
     * Finds a article by its id.
     * @return The matching article, otherwise null.
     * @throws SQLException
     */
    public List<Article> findByLabel(String label) {
        TypedQuery<Article> query = getEntityManager().createQuery("select p from Article p where p.label = :label " +
                "order by p.label", Article.class);
        query.setParameter("label", label);
        return query.getResultList();
    }

    public List<Article> findByArtistId(int id) {
        List <Article> articleList = new ArrayList<>();
        articleList.addAll(bookRepository.findByAuthorId(id));
        articleList.addAll(cdRepository.findByMemberId(id));
        articleList.addAll(dvdRepository.findByActorsId(id));
        articleList.addAll(dvdRepository.findByDirectorId(id));
        return articleList;
    }

}
