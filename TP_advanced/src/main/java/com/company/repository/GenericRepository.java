package com.company.repository;

import javax.persistence.EntityManager;
import com.company.helper.DataBaseHelper;

public class GenericRepository {

    private EntityManager entityManager;

    public GenericRepository() {
        this.entityManager = DataBaseHelper.createEntityManager();
    }

    protected EntityManager getEntityManager() {
        if (entityManager == null || !entityManager.isOpen()) {
            entityManager = DataBaseHelper.createEntityManager();
        }
        return entityManager;
    }

}
