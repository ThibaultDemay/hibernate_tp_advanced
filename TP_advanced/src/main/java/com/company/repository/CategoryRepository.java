package com.company.repository;

import com.company.exception.AlreadyExistsException;
import com.company.helper.DataBaseHelper;
import com.company.model.Category;
import com.company.model.Category;
import com.company.model.Category;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.sql.SQLException;
import java.util.List;

public class CategoryRepository extends GenericRepository {

    public Category insert(Category category) throws AlreadyExistsException {
        EntityManager entityManager = getEntityManager();
        List<Category> existingCategorys = findByLabel(category.getLabel());
        if (!existingCategorys.isEmpty()) {
            throw new AlreadyExistsException("The category named " + category.getLabel() + " already exists.");
        }
        DataBaseHelper.beginTransaction(entityManager);
        entityManager.persist(category);
        DataBaseHelper.commitTransactionAndClose(entityManager);
        return category;
    }

    public Category update(Category category) {
        EntityManager entityManager = getEntityManager();
        DataBaseHelper.beginTransaction(entityManager);
        entityManager.merge(category);
        DataBaseHelper.commitTransactionAndClose(entityManager);
        return category;
    }


    /**
     * Finds all categorys.
     *
     * @return A list containing all the categorys.
     */
    public List<Category> findAll() {
        return getEntityManager().createQuery("from Category order by id", Category.class).getResultList();
    }


    /**
     * Finds a category by its id.
     *
     * @return The matching category, otherwise null.
     * @throws SQLException
     */
    public Category findById(int id) {
        return getEntityManager().find(Category.class, id);
    }

    /**
     * Finds a category by its id.
     *
     * @return The matching category, otherwise null.
     * @throws SQLException
     */
    public List<Category> findByLabel(String label) {
        TypedQuery<Category> query = getEntityManager().createQuery("select p from Category p where p.label = :label " +
                "order by p.label", Category.class);
        query.setParameter("label", label);
        return query.getResultList();
    }
}
