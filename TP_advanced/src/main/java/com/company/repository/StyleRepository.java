package com.company.repository;

import com.company.exception.AlreadyExistsException;
import com.company.helper.DataBaseHelper;
import com.company.model.Cd;
import com.company.model.Style;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.sql.SQLException;
import java.util.List;

public class StyleRepository extends GenericRepository {

    public Style insert(Style style) throws AlreadyExistsException {
        EntityManager entityManager = getEntityManager();
        List<Style> existingStyles = findByLabel(style.getLabel());
        if (!existingStyles.isEmpty()) {
            throw new AlreadyExistsException("The style named " + style.getLabel() + " already exists.");
        }
        DataBaseHelper.beginTransaction(entityManager);
        entityManager.persist(style);
        DataBaseHelper.commitTransactionAndClose(entityManager);
        return style;
    }


    public Style update(Style style) {
        EntityManager entityManager = getEntityManager();
        DataBaseHelper.beginTransaction(entityManager);
        entityManager.merge(style);
        DataBaseHelper.commitTransactionAndClose(entityManager);
        return style;
    }


    /**
     * Finds all styles.
     *
     * @return A list containing all the styles.
     */
    public List<Style> findAll() {
        return getEntityManager().createQuery("from Style order by id", Style.class).getResultList();
    }


    /**
     * Finds a style by its id.
     *
     * @return The matching style, otherwise null.
     * @throws SQLException
     */
    public Style findById(int id) {
        return getEntityManager().find(Style.class, id);
    }

    /**
     * Finds a style by its id.
     *
     * @return The matching style, otherwise null.
     * @throws SQLException
     */
    public List<Style> findByLabel(String label) {
        TypedQuery<Style> query = getEntityManager().createQuery("select s from Style s where s.label = :label " +
                "order by s.label", Style.class);
        query.setParameter("label", label);
        return query.getResultList();
    }

    //TODO : Rajouter la quantité ici
    public double getSumSelling(Style style) {
        TypedQuery<Double> query = getEntityManager().createQuery("select sum(c.price * a.quantity) " +
                        "from Style s "
                        + "join s.cdList c "
                        + "join c.commandArticleList a "
                        + "where s = :style"
                , java.lang.Double.class);
        query.setParameter("style", style);
        return query.getSingleResult();
    }
}
