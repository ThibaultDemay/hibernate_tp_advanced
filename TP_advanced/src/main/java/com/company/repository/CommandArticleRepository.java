package com.company.repository;

import com.company.model.Book;
import com.company.model.Cd;
import com.company.model.Dvd;
import com.company.model.Style;

import javax.persistence.TypedQuery;

public class CommandArticleRepository extends GenericRepository {

    public double getSumSellingByArticleType(CommandRepository.ArticleType articleType) {
        TypedQuery<Double> query = getEntityManager().createQuery("select sum(ca.price * c.quantity)" +
                        "from CommandArticle c "
                        + "join c.article ca "
                        + "where TYPE(ca) = :articleType"
                //+"join ca."
                , java.lang.Double.class);
        if (articleType == CommandRepository.ArticleType.BOOK) {
            query.setParameter("articleType", Book.class);
        }
        if (articleType == CommandRepository.ArticleType.DVD) {
            query.setParameter("articleType", Dvd.class);
        }
        if (articleType == CommandRepository.ArticleType.CD) {
            query.setParameter("articleType", Cd.class);
        }
        return query.getSingleResult();

    }

    public double getSumStyleSelling(Style style) {
        TypedQuery<Double> query = getEntityManager().createQuery("select sum(a.price * c.quantity)" +
                        "from CommandArticle c "
                        + "join c.article a, Cd cd "
                        + "join cd.styleList sl "
                        + "where  cd = a and sl = :style"
                //+"join ca."
                , java.lang.Double.class);

            query.setParameter("style", style);

        return query.getSingleResult();

    }

}
