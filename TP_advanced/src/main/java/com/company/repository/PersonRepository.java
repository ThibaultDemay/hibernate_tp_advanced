package com.company.repository;

import com.company.exception.AlreadyExistsException;
import com.company.helper.DataBaseHelper;
import com.company.model.Person;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.sql.SQLException;
import java.util.List;

public class PersonRepository extends GenericRepository {

    public Person insert(Person person) throws AlreadyExistsException {
        EntityManager entityManager = getEntityManager();
        List<Person> existingPersons = findByNameAndFirstName(person.getName(), person.getFirstName());
        if (!existingPersons.isEmpty()) {
            throw new AlreadyExistsException("The person named " + person.getFirstName() + " " +person.getName() + " already exists.");
        }
        DataBaseHelper.beginTransaction(entityManager);
        entityManager.persist(person);
        DataBaseHelper.commitTransactionAndClose(entityManager);
        return person;
    }


    public Person update(Person person) {
        EntityManager entityManager = getEntityManager();
        DataBaseHelper.beginTransaction(entityManager);
        entityManager.merge(person);
        DataBaseHelper.commitTransactionAndClose(entityManager);
        return person;
    }



    /**
     * Finds all persons.
     * @return A list containing all the persons.
     */
    public List<Person> findAll() {
        return getEntityManager().createQuery("from Person order by id", Person.class).getResultList();
    }


    /**
     * Finds a person by its id.
     * @return The matching person, otherwise null.
     * @throws SQLException
     */
    public Person findById(int id) {
        return getEntityManager().find(Person.class, id);
    }

    /**
     * Finds a person by its id.
     * @return The matching person, otherwise null.
     * @throws SQLException
     */
    public List<Person> findByNameAndFirstName(String name, String firstName) {
        TypedQuery<Person> query = getEntityManager().createQuery("select p from Person p where p.name = :name " +
                "and p.firstName = :firstName order by p.name", Person.class);
        query.setParameter("name", name);
        query.setParameter("firstName", firstName);
        return query.getResultList();
    }



}
