package com.company.repository;

import com.company.exception.AlreadyExistsException;
import com.company.helper.DataBaseHelper;
import com.company.model.Band;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.sql.SQLException;
import java.util.List;

public class BandRepository extends GenericRepository {

    public Band insert(Band band) throws AlreadyExistsException {
        EntityManager entityManager = getEntityManager();
        //Band existing = findById(band.getId());
        List<Band> existingBands = findByName(band.getName());
        if (!existingBands.isEmpty()) {
            throw new AlreadyExistsException("The band named " + band.getName() + " already exists.");
        }
        DataBaseHelper.beginTransaction(entityManager);
        entityManager.persist(band);
        DataBaseHelper.commitTransactionAndClose(entityManager);
        return band;
    }


    public Band update(Band band) {
        EntityManager entityManager = getEntityManager();
        DataBaseHelper.beginTransaction(entityManager);
        entityManager.merge(band);
        DataBaseHelper.commitTransactionAndClose(entityManager);
        return band;
    }


    /**
     * Finds all bands.
     *
     * @return A list containing all the bands.
     */
    public List<Band> findAll() {
        return getEntityManager().createQuery("from Band order by id", Band.class).getResultList();
    }


    /**
     * Finds a band by its id.
     *
     * @return The matching band, otherwise null.
     * @throws SQLException
     */
    public Band findById(int id) {
        return getEntityManager().find(Band.class, id);
    }

    /**
     * Finds a band by its id.
     *
     * @return The matching band, otherwise null.
     * @throws SQLException
     */
    public List<Band> findByName(String name) {
        TypedQuery<Band> query = getEntityManager().createQuery("select p from Band p where p.name = :name " +
                "order by p.name", Band.class);
        query.setParameter("name", name);
        return query.getResultList();
    }

}


