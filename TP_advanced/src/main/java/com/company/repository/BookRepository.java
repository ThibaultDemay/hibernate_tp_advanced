package com.company.repository;

import com.company.exception.AlreadyExistsException;
import com.company.helper.DataBaseHelper;
import com.company.model.Book;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.sql.SQLException;
import java.util.List;

public class BookRepository extends GenericRepository {

    public Book insert(Book book) throws AlreadyExistsException {
        EntityManager entityManager = getEntityManager();
        //Book existing = findById(book.getId());
        List<Book> existingBooks = findByLabel(book.getLabel());
        if (!existingBooks.isEmpty()) {
            throw new AlreadyExistsException("The book labeled "+book.getLabel() + " already exists.");
        }
        DataBaseHelper.beginTransaction(entityManager);
        entityManager.persist(book);
        DataBaseHelper.commitTransactionAndClose(entityManager);
        return book;
    }


    public Book update(Book book) {
        EntityManager entityManager = getEntityManager();
        DataBaseHelper.beginTransaction(entityManager);
        entityManager.merge(book);
        DataBaseHelper.commitTransactionAndClose(entityManager);
        return book;
    }



    /**
     * Finds all books.
     * @return A list containing all the books.
     */
    public List<Book> findAll() {
        return getEntityManager().createQuery("select b from Book b order by b.id", Book.class).getResultList();
    }


    /**
     * Finds a book by its id.
     * @return The matching book, otherwise null.
     * @throws SQLException
     */
    public Book findById(int id) {
        return getEntityManager().find(Book.class, id);
    }

    /**
     * Finds a book by its id.
     * @return The matching book, otherwise null.
     * @throws SQLException
     */
    public List<Book> findByLabel(String label) {
        TypedQuery<Book> query = getEntityManager().createQuery("select p from Book p where p.label = :label " +
                "order by p.label", Book.class);
        query.setParameter("label", label);
        return query.getResultList();
    }

    public List<Book> findByAuthorId(int authorId){
        TypedQuery<Book> query = getEntityManager().createQuery("select b from Book b join b.authorList ba where ba.id = :authorId " +
                "order by b.label", Book.class);
        query.setParameter("authorId", authorId);
        return query.getResultList();
    }

}
