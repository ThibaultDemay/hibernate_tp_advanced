package com.company.repository;

import com.company.exception.AlreadyExistsException;
import com.company.helper.DataBaseHelper;
import com.company.model.Cd;
import com.company.model.Style;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.sql.SQLException;
import java.util.List;

public class CdRepository extends GenericRepository {

    public Cd insert(Cd cd) throws AlreadyExistsException {
        EntityManager entityManager = getEntityManager();
        //Cd existing = findById(cd.getId());
        List<Cd> existingCds = findByLabel(cd.getLabel());
        if (!existingCds.isEmpty()) {
            throw new AlreadyExistsException("The cd labeled "+cd.getLabel() + " already exists.");
        }
        DataBaseHelper.beginTransaction(entityManager);
        entityManager.persist(cd);
        DataBaseHelper.commitTransactionAndClose(entityManager);
        return cd;
    }


    public Cd update(Cd cd) {
        EntityManager entityManager = getEntityManager();
        DataBaseHelper.beginTransaction(entityManager);
        entityManager.merge(cd);
        DataBaseHelper.commitTransactionAndClose(entityManager);
        return cd;
    }


    /**
     * Finds all cds.
     * @return A list containing all the cds.
     */
    public List<Cd> findAll() {
        return getEntityManager().createQuery("from Cd order by id", Cd.class).getResultList();
    }


    /**
     * Finds a cd by its id.
     * @return The matching cd, otherwise null.
     * @throws SQLException
     */
    public Cd findById(int id) {
        return getEntityManager().find(Cd.class, id);
    }

    /**
     * Finds a cd by its id.
     * @return The matching cd, otherwise null.
     * @throws SQLException
     */
    public List<Cd> findByLabel(String label) {
        TypedQuery<Cd> query = getEntityManager().createQuery("select c from Cd c where c.label = :label " +
                "order by c.label", Cd.class);
        query.setParameter("label", label);
        return query.getResultList();
    }


    public List<Cd> findByMemberId(int memberId){
//        TypedQuery<Cd> query = getEntityManager().createQuery("select c from Cd c join c.bandList b join b.memberList m where m.id = :memberId " +
//                "order by c.label", Cd.class);
        TypedQuery<Cd> query = getEntityManager().createQuery("select c from Cd c join c.bandList b join b.memberList m where m.id = :memberId " +
                "order by c.label", Cd.class);

        query.setParameter("memberId", memberId);
        return query.getResultList();
    }



}
