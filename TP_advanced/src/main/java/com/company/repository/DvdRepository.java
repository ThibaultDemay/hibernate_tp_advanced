package com.company.repository;

import com.company.exception.AlreadyExistsException;
import com.company.helper.DataBaseHelper;
import com.company.model.Dvd;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.sql.SQLException;
import java.util.List;

public class DvdRepository extends GenericRepository {

    public Dvd insert(Dvd dvd) throws AlreadyExistsException {
        EntityManager entityManager = getEntityManager();
        //Dvd existing = findById(dvd.getId());
        List<Dvd> existingDvds = findByLabel(dvd.getLabel());
        if (!existingDvds.isEmpty()) {
            throw new AlreadyExistsException("The dvd labeled "+dvd.getLabel() + " already exists.");
        }
        DataBaseHelper.beginTransaction(entityManager);
        entityManager.persist(dvd);
        DataBaseHelper.commitTransactionAndClose(entityManager);
        return dvd;
    }


    public Dvd update(Dvd dvd) {
        EntityManager entityManager = getEntityManager();
        DataBaseHelper.beginTransaction(entityManager);
        entityManager.merge(dvd);
        DataBaseHelper.commitTransactionAndClose(entityManager);
        return dvd;
    }



    /**
     * Finds all dvds.
     * @return A list containing all the dvds.
     */
    public List<Dvd> findAll() {
        return getEntityManager().createQuery("from Dvd order by id", Dvd.class).getResultList();
    }


    /**
     * Finds a dvd by its id.
     * @return The matching dvd, otherwise null.
     * @throws SQLException
     */
    public Dvd findById(int id) {
        return getEntityManager().find(Dvd.class, id);
    }

    /**
     * Finds a dvd by its id.
     * @return The matching dvd, otherwise null.
     * @throws SQLException
     */
    public List<Dvd> findByLabel(String label) {
        TypedQuery<Dvd> query = getEntityManager().createQuery("select p from Dvd p where p.label = :label " +
                "order by p.label", Dvd.class);
        query.setParameter("label", label);
        return query.getResultList();
    }

    public List<Dvd> findByDirectorId(int directorId){
        TypedQuery<Dvd> query = getEntityManager().createQuery("select d from Dvd d join d.director dd where dd.id = :directorId " +
                "order by d.label", Dvd.class);
        query.setParameter("directorId", directorId);
        return query.getResultList();
    }

    public List<Dvd> findByActorsId(int actorId){
        TypedQuery<Dvd> query = getEntityManager().createQuery("select d from Dvd d join d.actors da where da.id = :actorId " +
                "order by d.label", Dvd.class);
        query.setParameter("actorId", actorId);
        return query.getResultList();
    }

}
