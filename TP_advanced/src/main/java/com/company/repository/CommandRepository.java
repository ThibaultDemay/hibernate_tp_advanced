package com.company.repository;

import com.company.exception.AlreadyExistsException;
import com.company.helper.DataBaseHelper;
import com.company.model.Command;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.sql.SQLException;
import java.util.List;

public class CommandRepository extends GenericRepository {

    public enum ArticleType {
        BOOK, CD, DVD
    }

    public Command insert(Command command) throws AlreadyExistsException {
        EntityManager entityManager = getEntityManager();
        List<Command> existingCommands = findByLabel(command.getLabel());
        if (!existingCommands.isEmpty()) {
            throw new AlreadyExistsException("The command named " + command.getLabel() + " already exists.");
        }
        DataBaseHelper.beginTransaction(entityManager);
        entityManager.persist(command);
        DataBaseHelper.commitTransactionAndClose(entityManager);
        return command;
    }

    public Command update(Command command) {
        EntityManager entityManager = getEntityManager();
        DataBaseHelper.beginTransaction(entityManager);
        entityManager.merge(command);
        DataBaseHelper.commitTransactionAndClose(entityManager);
        return command;
    }


    /**
     * Finds all commands.
     *
     * @return A list containing all the commands.
     */
    public List<Command> findAll() {
        return getEntityManager().createQuery("select c from Command c order by c.id", Command.class).getResultList();
    }


    /**
     * Finds a command by its id.
     *
     * @return The matching command, otherwise null.
     * @throws SQLException
     */
    public Command findById(int id) {
        return getEntityManager().find(Command.class, id);
    }

    /**
     * Finds a command by its id.
     *
     * @return The matching command, otherwise null.
     * @throws SQLException
     */
    public List<Command> findByLabel(String label) {
        TypedQuery<Command> query = getEntityManager().createQuery("select c from Command c where c.label = :label " +
                "order by c.label", Command.class);
        query.setParameter("label", label);
        return query.getResultList();
    }

    public double getSumSellingByArticleType(ArticleType articleType) {
        if (articleType == ArticleType.BOOK) {
            TypedQuery<java.lang.Double> query = getEntityManager().createQuery("select sum(ca.price)" +
                            "from Command c "
                            + "join c.commandArticleList ca "
                    //+"join ca."
                    //+"join Book b "
                    , java.lang.Double.class);
            return query.getSingleResult();
        }
        return (double) (1);
    }


}
