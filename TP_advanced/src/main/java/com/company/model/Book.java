package com.company.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Book extends Article {

    @ManyToMany
    @JoinTable(name = "book_person",
            joinColumns = @JoinColumn(
                    name = "book_id"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "person_id"
            )
    )
    List<Person> authorList;

    public Book(String label, List<Person> personList, Float price) {
        this.setLabel(label);
        this.authorList = personList;
        this.setPrice(price);
    }

    public Book() {
    }

    public List<Person> getAuthorList() {
        return authorList;
    }

    public void setAuthorList(List<Person> authorList) {
        this.authorList = authorList;
    }
}
