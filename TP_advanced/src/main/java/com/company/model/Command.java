package com.company.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "command")
public class Command {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "label_")
    private String label;
    @OneToMany(mappedBy = "command", cascade = CascadeType.ALL)
    private Set<CommandArticle> commandArticleList;

    public Command() {

    }

    public Command(String label, CommandArticle... commandArticles) {
        this.label = label;
        for (CommandArticle ca : commandArticles){
            ca.setCommand(this);
        }
        this.commandArticleList = Stream.of(commandArticles).collect(Collectors.toSet());
    }


    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        label = label;
    }
}

