package com.company.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Band {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(name = "name_")
    String name;

    @ManyToMany
    @JoinTable(name = "band_person",
            joinColumns = @JoinColumn(
                    name = "band_id"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "person_id"
            )
    )
    List<Person>memberList;

    @ManyToMany(mappedBy = "bandList")
    List<Cd>cdList;

    public Band(){

    }

    public Band(String name, List<Person> personList){
        this.name = name;
        this.memberList = personList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Person> getMemberList() {
        return memberList;
    }

    public void setMemberList(List<Person> memberList) {
        this.memberList = memberList;
    }

    public List<Cd> getCdList() {
        return cdList;
    }

    public void setCdList(List<Cd> cdList) {
        this.cdList = cdList;
    }
}
