package com.company.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "command_article")
public class CommandArticle implements Serializable {
    @Id
    @ManyToOne
    @JoinColumn
    private Command command;
    @Id
    @ManyToOne
    @JoinColumn
    private Article article;

    private Integer quantity;

    public CommandArticle(Article article, Integer quantity) {
        this.article = article;
        this.quantity = quantity;
    }

    public CommandArticle(){

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CommandArticle)) return false;
        CommandArticle that = (CommandArticle) o;
        return Objects.equals(article.getLabel(), that.article.getLabel()) &&
                Objects.equals(command.getLabel(), that.command.getLabel()) &&
                Objects.equals(quantity, that.quantity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(article.getLabel(), command.getLabel(), quantity);
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
