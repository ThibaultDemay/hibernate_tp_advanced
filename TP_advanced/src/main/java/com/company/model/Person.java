package com.company.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(name = "name_")
    private String name;
    @Column(name = "firstName_")
    private String firstName;
    @ManyToMany(mappedBy = "authorList")
    private List<Book> bookList;
    @ManyToMany(mappedBy = "memberList")
    private List<Band>bandList;


    public Person(){

    }

    public Person(String name, String firstName){
        this.name = name;
        this.firstName = firstName;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public List<Book> getBookList() {
        return bookList;
    }

    public void setBookList(List<Book> bookList) {
        this.bookList = bookList;
    }

    public List<Band> getBandList() {
        return bandList;
    }

    public void setBandList(List<Band> bandList) {
        this.bandList = bandList;
    }

    @Override
    public String toString() {
        return this.name + " " + this.firstName;
    }
}
