package com.company.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Dvd extends Article {

    @ManyToOne(fetch= FetchType.LAZY)
    private Category category;

    @ManyToMany
    @JoinTable(name = "dvd_actors",
            joinColumns = @JoinColumn(
                    name = "dvd_id"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "person_id"
            )
    )
    private List<Person> actors;

    @ManyToOne(fetch= FetchType.LAZY)
    private Person director;


    public Dvd(String label, float price, List<Person> actors, Person director, Category category)
    {
        this.actors = actors;
        this.director= director;
        this.setLabel(label);
        this.category = category;
        this.setPrice(price);
    }


    public Dvd(){
    }
}
