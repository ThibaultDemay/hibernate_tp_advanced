package com.company.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(name = "label_")
    String label;

    @OneToMany(fetch = FetchType.LAZY)
    private List<Dvd> dvdList;

    public Category(){
        this.label = null;
    }

    public Category(String label){
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
