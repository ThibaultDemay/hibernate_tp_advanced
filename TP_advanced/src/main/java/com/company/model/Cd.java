package com.company.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Cd extends Article {

    @ManyToMany
    @JoinTable(name = "cd_band",
            joinColumns = @JoinColumn(
                    name = "cd_id"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "band_id"
            )
    )
    private List<Band> bandList;

    @ManyToMany
    @JoinTable(name = "cd_style",
            joinColumns = @JoinColumn(
                    name = "cd_id"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "style_id"
            )
    )
    private List<Style> styleList;

    public Cd(String label, float price, List<Band> bandList, List<Style> styleList) {
        this.bandList = bandList;
        this.styleList = styleList;
        this.setLabel(label);
        this.setPrice(price);
    }

    public Cd() {
    }


}


