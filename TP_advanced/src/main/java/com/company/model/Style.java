package com.company.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Style {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(name = "label_")
    String label;

    @ManyToMany(mappedBy = "styleList")
    private List<Cd> cdList;

    public Style(String label) {
        this.label = label;
    }

    public Style(){
        this.label = null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<Cd> getCdList() {
        return cdList;
    }

    public void setCdList(List<Cd> cdList) {
        this.cdList = cdList;
    }
}
