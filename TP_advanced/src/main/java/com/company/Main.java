package com.company;
import com.company.exception.AlreadyExistsException;
import com.company.helper.DataBaseHelper;
import com.company.helper.DataBaseInserter;
import com.company.model.Article;
import com.company.model.CommandArticle;
import com.company.model.Person;
import com.company.repository.*;

import javax.persistence.Persistence;
import java.util.List;

public class Main {

    public static void main(String[] args) throws AlreadyExistsException {

        // Bonjour Salim !
        // Voici mon TP, réalisé en solo !

        DataBaseHelper.createEntityManager();
        DataBaseInserter dbInserter = new DataBaseInserter(); // Une petite classe dédiée au remplissage de la BDD
        dbInserter.fillDataBase(); // A voir donc dans DataBaseInserter pour voir les différents éléments créés

        // J'invoque quelques repositories pour m'aider à affronter les requests mob of Death
        PersonRepository personRepository = new PersonRepository(); // force jaune
        ArticleRepository articleRepository = new ArticleRepository(); // force bleue
        CommandArticleRepository commandArticleRepository = new CommandArticleRepository(); // force rouge
        StyleRepository styleRepository = new StyleRepository(); //Force noire


        //region REQUETE AVANCEE 1 : LA TOTALITE DES OEUVRES D'UN ARTISTE

        // Pour cette première quête, j'invoque le maître de Providence, sir HPL
        Person lovecraft = personRepository.findById(1);
        // J'utilise le articleRepository et sa méthode findByArtist.
        List<Article> oeuvresDeLovecraft = articleRepository.findByArtistId(1); // Function custom dans l'articleRepo
        // C'est une méthode composite qui fait appel aux trois repositories de cd, book et dvd
        // C'est probablement faisable en une seule requête combinée, mais je n'ai pas le skill (et le temps ! :D )
        System.out.println("Les oeuvres de " + lovecraft + " sont : " + oeuvresDeLovecraft);
        // Si tout s'est bien passé, on retrouve trois oeuvres pour HPL :
        // Le livre : la couleur tombée du ciel dont il est auteur
        // Le CD fictif : L'appel de Cthulhu en La mineur Septième Quinte Bémol, qu'il a composé avec Django Reinhardt, c'est du jazz manouche cosmique
        // Le DVD : The Thing, où il est acteur (caché dans le monstre).
        // Le DVD à venir : La couleur tombée du ciel, le film ! dont il est réalisateur.

       //endregion


        //region REQUETE AVANCEE 2 : la somme (prix) des ventes effectuées par catégorie d’articles
        CommandRepository.ArticleType typeFromEnum = CommandRepository.ArticleType.BOOK;
        System.out.println("Le total des ventes de livres : " + commandArticleRepository.getSumSellingByArticleType(typeFromEnum));
        typeFromEnum = CommandRepository.ArticleType.CD;
        System.out.println("Le total des ventes de cds : " + commandArticleRepository.getSumSellingByArticleType(typeFromEnum));
        typeFromEnum = CommandRepository.ArticleType.DVD;
        System.out.println("Le total des ventes de dvds : " + commandArticleRepository.getSumSellingByArticleType(typeFromEnum));
        //endregion


        //region REQUETE AVANCEE 3 : Quel style musical connait les meilleurs ventes

        // tentative par le style Repository
//        Double venteBlues = styleRepository.getSumSelling(styleRepository.findById(1));
//        Double venteJazz = styleRepository.getSumSelling(styleRepository.findById(2));
//        Double venteJazzDeathPapillon = styleRepository.getSumSelling(styleRepository.findById(3));

        // tentative par le command Repository
        Double venteBlues = commandArticleRepository.getSumStyleSelling(styleRepository.findById(1));
        Double venteJazz = commandArticleRepository.getSumStyleSelling(styleRepository.findById(2));
        Double venteJazzDeathPapillon = commandArticleRepository.getSumStyleSelling(styleRepository.findById(3));
        System.out.println("Le style intitulé " +styleRepository.findById(1).getLabel() +" a fait " +venteBlues );
        System.out.println("Le style intitulé " +styleRepository.findById(2).getLabel() +" a fait " +venteJazz);
        System.out.println("Le style intitulé " +styleRepository.findById(3).getLabel() +" a fait " +venteJazzDeathPapillon );
        //endregion
    }
}